<?php
if (!class_exists('DB')) {
    class DB
    {
        public function __construct($user, $password, $database, $host)
        {
            $this->user = $user;
            $this->password = $password;
            $this->database = $database;
            $this->host = $host;
        }
        public function connect()
        {
            return (new mysqli($this->host, $this->user, $this->password, $this->database));
        }
        public function query($query)
        {
            $db = $this->connect();
            $result = $db->query($query);
            return $result ? true : 0;
        }
        public function fetch($query)
        {
            $db = $this->connect();
            $result = $db->query($query);
            while ($row = mysqli_fetch_array($result)) {
                $results[] = $row;
            }
            return $results ? $results : 0;
        }
        public function get($query)
        {
            $db = $this->connect();
            $result = $db->query($query);
            $row = mysqli_fetch_array($result);
            return $row ? $row : 0;
        }
        public function rowCount($query)
        {
            $db = $this->connect();
            $rowcount = mysqli_num_rows($db->query($query));
            return ($db->query($query) ? $rowcount : 0);
        }

    }

}
$connection = new DB(DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST);

// if ($connection->connect()) {
//     echo 'Store Connection Failed';
//     exit;
//     die;
// }
//date_default_timezone_set("Asia/Calcutta");
//session_start();
//session_destroy();